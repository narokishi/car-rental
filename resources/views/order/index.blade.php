@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('order.index') }}
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="offset-4 col-md-4">
                    <a class="btn btn-success btn-block" href="{{ route('order.create') }}">Dodaj nowe zamówienie</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-1">LP.</th>
                        <th>Najemca</th>
                        <th>Samochód</th>
                        <th>Wynajem od dnia</th>
                        <th>Wynajem do dnia</th>
                        <th class="col-1"></th>
                        <th class="col-1"></th>
                        <th class="col-1"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                <a href="{{ route('client.show', $order->borrower) }}">
                                    <strong>{{ $order->borrower->name }}</strong>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('vehicle.show', $order->vehicle) }}">
                                    <strong>{{ $order->vehicle->name }}</strong>
                                </a>
                            </td>
                            <td>{{ $order->borrowed_to }}</td>
                            <td>{{ $order->borrowed_from }}</td>
                            <td>
                                <a class="btn btn-warning btn-block" href="{{ route('order.show', $order) }}">Zobacz</a>
                            </td>
                            <td>
                                <a class="btn btn-primary btn-block" href="{{ route('order.edit', $order) }}">Edytuj</a>
                            </td>
                            <td>
                                <a class="btn btn-danger btn-block" href="{{ route('order.destroy', $order) }}">Usuń</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $orders->links() }}
        </div>
    </div>
</div>
@endsection
