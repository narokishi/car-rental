<div class="col-6">
    <div class="form-group">
        {{ Form::label('vehicle_id', 'Pojazd', [
            'class' => 'form-label',
        ]) }}
        {{ Form::select('vehicle_id', $vehicles, null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-6">
    <div class="form-group">
        {{ Form::label('borrower_id', 'Najemca', [
            'class' => 'form-label',
        ]) }}
        {{ Form::select('borrower_id', $clients, null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-6">
    <div class="form-group">
        {{ Form::label('borrowed_from', 'Wynajem od', [
            'class' => 'form-label',
        ]) }}
        <div class="input-group">
            {{ Form::date('borrowed_from[date]', null, [
                'class'    => 'form-control',
                'required' => 'required',
            ]) }}
            <div class="input-group-addon">
                {{ Form::time('borrowed_from[time]', null, [
                    'class'    => 'form-control',
                    'required' => 'required',
                ]) }}
            </div>
        </div>
    </div>
</div>
<div class="col-6">
    <div class="form-group">
        {{ Form::label('borrowed_to', 'Wynajem do', [
            'class' => 'form-label',
        ]) }}
        <div class="input-group">
            {{ Form::date('borrowed_to[date]', null, [
                'class'    => 'form-control',
                'required' => 'required',
            ]) }}
            <div class="input-group-addon">
                {{ Form::time('borrowed_to[time]', null, [
                    'class'    => 'form-control',
                    'required' => 'required',
                ]) }}
            </div>
        </div>
    </div>
</div>
