@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('order.edit', $order) }}
    <div class="card">
        <div class="card-body">
            {{ Form::model($order, [
                'route'  => ['order.update', $order],
                'method' => 'PUT',
            ]) }}
                <div class="row">
                    @include('order.inputs')
                    @include('include.submit-btn', [
                        'text' => 'Zapisz »',
                    ])
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
