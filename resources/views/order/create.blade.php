@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('order.create') }}
    <div class="card">
        <div class="card-body">
            {{ Form::open([
                'route' => 'order.store',
            ]) }}
                <div class="row">
                    @include('order.inputs')
                    <div class="offset-3 col-3">
                        <a class="btn btn-primary btn-block" href="{{ URL::previous() }}">« Wróć</a>
                    </div>
                    <div class="col-3">
                        {{ Form::button('Dodaj »', [
                            'class' => 'btn btn-success btn-block',
                            'type'  => 'submit',
                        ]) }}
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
