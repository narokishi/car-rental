@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('client.create') }}
    <div class="card">
        <div class="card-body">
            {{ Form::open([
                'route' => 'client.store',
            ]) }}
                <div class="row">
                    @include('client.form-inputs')
                    @include('include.submit-btn', [
                        'text' => 'Dodaj »',
                    ])
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
