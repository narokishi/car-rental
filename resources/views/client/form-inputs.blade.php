<div class="col-6">
    <div class="form-group">
        {{ Form::label('first_name', 'Imię', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('first_name', null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-6">
    <div class="form-group">
        {{ Form::label('last_name', 'Nazwisko', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('last_name', null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-6">
    <div class="form-group">
        {{ Form::label('pesel', 'PESEL', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('pesel', null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-3">
    <div class="form-group">
        {{ Form::label('email', 'Adres email', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('email', null, [
            'class' => 'form-control',
        ]) }}
    </div>
</div>
<div class="col-3">
    <div class="form-group">
        {{ Form::label('phone_number', 'Numer kontaktowy', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('phone_number', null, [
            'class' => 'form-control',
        ]) }}
    </div>
</div>
<div class="col-3">
    <div class="form-group">
        {{ Form::label('postal_code', 'Kod pocztowy', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('postal_code', null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-3">
    <div class="form-group">
        {{ Form::label('city', 'Miasto', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('city', null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-3">
    <div class="form-group">
        {{ Form::label('address_1', 'Ulica', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('address_1', null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-2">
    <div class="form-group">
        {{ Form::label('address_2', 'Nr domu', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('address_2', null, [
            'class'    => 'form-control',
            'required' => 'required',
        ]) }}
    </div>
</div>
<div class="col-1">
    <div class="form-group">
        {{ Form::label('address_3', 'Nr lokalu', [
            'class' => 'form-label',
        ]) }}
        {{ Form::text('address_3', null, [
            'class' => 'form-control',
        ]) }}
    </div>
</div>
