@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('client.show', $client) }}
    <div class="card">
        <div class="card-body">
            <h1 class="text-center">{{ $client->name }}</h1>
            @if($client->vehicles->isNotEmpty())
                <table class="table table-striped table-hover">
                    <i>Właściciel poniższych samochodów</i>
                    <thead>
                        <tr>
                            <th>Marka i model</th>
                            <th>Data produkcji</th>
                            <th>Numer rejestracyjny</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($client->vehicles as $vehicle)
                            <tr>
                                <td>
                                    <a href="{{ route('vehicle.show', $vehicle) }}">
                                        <strong>{{ $vehicle->name }}</strong>
                                    </a>
                                </td>
                                <td>{{ $vehicle->manufactured_at }}</td>
                                <td>{{ $vehicle->plate_number }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            @if($client->orders->isNotEmpty())
                <table class="table table-striped table-hover">
                    <i>Zamówienia</i>
                    <thead>
                        <tr>
                            <th>Marka i model</th>
                            <th>Numer rejestracyjny</th>
                            <th>Data wynajmu</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($client->orders as $order)
                            <tr>
                                <td>
                                    <a href="{{ route('vehicle.show', $order->vehicle) }}">
                                        <strong>{{ $order->vehicle->name }}</strong>
                                    </a>
                                </td>
                                <td>{{ $order->vehicle->plate_number }}</td>
                                <td>{{ $order->dates }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            <div class="row">
                <div class="offset-4 col-4">
                    <a class="btn btn-primary btn-block" href="{{ route('client.edit', $client) }}">Edytuj »</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
