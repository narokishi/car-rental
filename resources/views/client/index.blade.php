@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('client.index') }}
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="offset-4 col-md-4">
                    <a class="btn btn-success btn-block" href="{{ route('client.create') }}">Dodaj nowego klienta</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-1">LP.</th>
                        <th>Imię i nazwisko</th>
                        <th>Adres</th>
                        <th>Kontakt mailowy</th>
                        <th>Kontakt telefoniczny</th>
                        <th class="col-1"></th>
                        <th class="col-1"></th>
                        <th class="col-1"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $client->name }}</td>
                            <td>{{ $client->address }}</td>
                            <td>{{ $client->email }}</td>
                            <td>{{ $client->phone_number }}</td>
                            <td>
                                <a class="btn btn-warning btn-block" href="{{ route('client.show', $client) }}">Zobacz</a>
                            </td>
                            <td>
                                <a class="btn btn-primary btn-block" href="{{ route('client.edit', $client) }}">Edytuj</a>
                            </td>
                            <td>
                                <a class="btn btn-danger btn-block" href="{{ route('client.destroy', $client) }}">Usuń</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $clients->links() }}
        </div>
    </div>
</div>
@endsection
