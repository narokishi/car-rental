@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('client.edit', $client) }}
    <div class="card">
        <div class="card-body">
            {{ Form::model($client, [
                'route'  => ['client.update', $client],
                'method' => 'PUT',
            ]) }}
                <div class="row">
                    @include('client.form-inputs')
                    @include('include.submit-btn', [
                        'text' => 'Zapisz »',
                    ])
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
