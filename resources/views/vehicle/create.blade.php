@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('vehicle.create') }}
    <div class="card">
        <div class="card-body">
            {{ Form::open([
                'route' => 'vehicle.store',
            ]) }}
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        {{ Form::label('brand', 'Marka', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::text('brand', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        {{ Form::label('model', 'Model', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::text('model', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        {{ Form::label('type', 'Typ pojazdu', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::select('type', $types, null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        {{ Form::label('vin', 'Numer VIN', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::text('vin', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        {{ Form::label('manufactured_at', 'Data produkcji', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::date('manufactured_at', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        {{ Form::label('plate_number', 'Numer rejestracyjny', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::text('plate_number', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        {{ Form::label('subtype', 'Typ nadwozia', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::text('subtype', null, [
                            'class' => 'form-control',
                        ]) }}
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        {{ Form::label('colour', 'Kolor nadwozia', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::text('colour', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        {{ Form::label('number_of_doors', 'Liczba drzwi', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::number('number_of_doors', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        {{ Form::label('number_of_seats', 'Liczba miejsc', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::number('number_of_seats', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        {{ Form::label('engine_id', 'Silnik', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::select('engine_id', $engines, null, [
                            'class'    => 'form-control',
                        ]) }}
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        {{ Form::label('mileage', 'Przebieg', [
                            'class' => 'form-label',
                        ]) }}
                        {{ Form::number('mileage', null, [
                            'class'    => 'form-control',
                            'required' => 'required',
                        ]) }}
                    </div>
                </div>
                @include('include.submit-btn', [
                    'text' => 'Dodaj »',
                ])
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
