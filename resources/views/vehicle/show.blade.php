@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('vehicle.show', $vehicle) }}
    <div class="card">
        <div class="card-body">
            <table class="table table-striped table-hover">
                <tbody>
                    <tr>
                        <th class="col-4" scope="row">Marka</th>
                        <td>{{ $vehicle->brand }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Model</th>
                        <td>{{ $vehicle->model }}</td>
                    </tr>
                    <tr>
                        <th scope="row">VIN</th>
                        <td>{{ $vehicle->vin }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Numer rejestracyjny</th>
                        <td>{{ $vehicle->plate_number }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Data produkcji</th>
                        <td>{{ $vehicle->manufactured_at}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Przebieg</th>
                        <td>{{ $vehicle->mileage }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Kolor</th>
                        <td>{{ $vehicle->colour }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Liczba drzwi / miejsc</th>
                        <td>{{ $vehicle->number_of_doors }} / {{ $vehicle->number_of_seats }}</td>
                    </tr>
                </tbody>
            </table>
            <div class="row">
                <div class="offset-4 col-4">
                    <a class="btn btn-primary btn-block" href="{{ route('vehicle.edit', $vehicle) }}">Edytuj »</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
