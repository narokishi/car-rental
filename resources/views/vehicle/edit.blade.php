@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('vehicle.edit', $vehicle) }}
    <div class="card">
        <div class="card-body">
            {{ Form::model($vehicle, [
                'route'  => ['vehicle.update', $vehicle],
                'method' => 'PUT',
            ]) }}
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            {{ Form::label('owner_id', 'Właściciel', [
                                'class' => 'form-label',
                            ]) }}
                            {{ Form::select('owner_id', $clients, null, [
                                'class'    => 'form-control',
                                'required' => 'required',
                            ]) }}
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            {{ Form::label('mileage', 'Przebieg', [
                                'class' => 'form-label',
                            ]) }}
                            {{ Form::number('mileage', null, [
                                'class'    => 'form-control',
                                'required' => 'required',
                            ]) }}
                        </div>
                    </div>
                    @include('include.submit-btn', [
                        'text' => 'Zapisz »',
                    ])
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
