@extends('layouts.app')
@section('content')
<div class="container">
    {{ Breadcrumbs::render('vehicle.index') }}
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="offset-2 col-4">
                    <div class="input-group">
                        {{ Form::text('brand', null, [
                            'class'        => 'form-control js-brand',
                            'placeholder'  => 'Wyszukaj po marce...',
                            'autocomplete' => 'off',
                        ]) }}
                        <span class="input-group-btn">
                            <a href="#" class="btn btn-info js-clear">Wyczyść</a>
                        </span>
                    </div>
                    <div class="dropdown">
                        <div class="dropdown-menu js-results"></div>
                    </div>
                </div>
                <div class="col-4">
                    <a class="btn btn-success btn-block" href="{{ route('vehicle.create') }}">Dodaj nowy samochód</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th class="col-1">LP.</th>
                        <th>Marka/Model</th>
                        <th>Właściciel</th>
                        <th>Najemca</th>
                        <th>Data wynajmu</th>
                        <th class="col-1"></th>
                        <th class="col-1"></th>
                        <th class="col-1"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($vehicles as $vehicle)
                        <tr>
                            <td class="align-middle">{{ $loop->iteration }}</td>
                            <td class="align-middle">{{ $vehicle->brand }} {{ $vehicle->model}}</td>
                            <td class="align-middle">
                                @if($vehicle->owner)
                                    <a href="{{ route('client.show', $vehicle->owner) }}">
                                        <strong>{{ $vehicle->owner->name }}</strong>
                                    </a>
                                @endif
                            </td>
                            <td class="align-middle">
                                @if($vehicle->borrower)
                                    <a href="{{ route('client.show', $vehicle->borrower) }}">
                                        <strong>{{ $vehicle->borrower->name }}</strong>
                                    </a>
                                @endif
                            </td>
                            <td class="align-middle">
                                @if($vehicle->isBorrowed())
                                    <a href="{{ route('order.show', $vehicle->order) }}">
                                        <strong>{{ $vehicle->order->dates }}</strong>
                                    </a>
                                @endif
                            </td>
                            <td class="align-middle">
                                <a class="btn btn-warning btn-block" href="{{ route('vehicle.show', $vehicle) }}">Zobacz</a>
                            </td>
                            <td class="align-middle">
                                <a class="btn btn-primary btn-block" href="{{ route('vehicle.edit', $vehicle) }}">Edytuj</a>
                            </td>
                            <td class="align-middle">
                                <a class="btn btn-danger btn-block" href="{{ route('vehicle.destroy', $vehicle) }}">Usuń</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $vehicles->links() }}
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        const brand = $('.js-brand');
        const reset = $('.js-reset');
        brand.on('input', function () {
            const brand = $('.js-brand').val();
            axios.get(
                '/api/vehicle/index/' + brand
            ).then(function (response) {
                const data = response.data;
                const results = $('.js-results');
                results.empty();
                results.addClass('show');
                $.each(data, function (index, value) {
                    results.append(`<a class="dropdown-item" href="?brand=${index}">${index} (${value} wyników)</a>`);
                });
                if (results.is(':empty')) {
                    results.append('<span class="dropdown-item">Brak wyników</span>')
                }
            }).catch(function (error) {
                console.log(error);
            });
        });
        brand.keyup(function() {
            if (!this.value) {
                const results = $('.js-results');
                results.empty();
                results.removeClass('show');
            }
        });
        $('.js-clear').on('click', function () {
            const url = document.location.href;
            window.location.replace(url.split('?')[0]);
        });
    });
</script>
@endpush
