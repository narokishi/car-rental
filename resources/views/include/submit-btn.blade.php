<div class="offset-4 col-4">
    {{ Form::button($text, [
        'class' => 'btn btn-success btn-block',
        'type'  => 'submit',
    ]) }}
</div>
