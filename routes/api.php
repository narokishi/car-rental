<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.vehicle.index')->get('vehicle/index/{brand}', function ($brand) {
    $vehicles = \App\Vehicle::where('brand', 'like', "%$brand%")->get(['brand']);

    return $vehicles->groupBy('brand')->map(function ($vehicles) {
        return $vehicles->count();
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
