<?php

foreach([
    'vehicle' => 'Samochody',
    'order'   => 'Zamówienia',
    'client'  => 'Klienci',
] as $key => $value) {
    Breadcrumbs::register("{$key}.index", function($breadcrumbs) use ($value, $key) {
        $breadcrumbs->push($value, route("{$key}.index"));
    });

    Breadcrumbs::register("{$key}.create", function($breadcrumbs) use ($key) {
        $breadcrumbs->parent("{$key}.index");
        $breadcrumbs->push('Dodawanie', route("{$key}.create"));
    });

    Breadcrumbs::register("{$key}.show", function($breadcrumbs, $model) use ($key) {
        $breadcrumbs->parent("{$key}.index");
        $breadcrumbs->push($model->name, route("{$key}.show", $model));
    });

    Breadcrumbs::register("{$key}.edit", function($breadcrumbs, $model) use ($key) {
        $breadcrumbs->parent("{$key}.show", $model);
        $breadcrumbs->push('Modyfikacja', route("{$key}.edit", $model));
    });
}
