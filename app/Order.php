<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
    protected $fillable = [
        'borrower_id',
        'vehicle_id',
        'borrowed_from',
        'borrowed_to',
    ];

    protected $dates = [
        'borrowed_from',
        'borrowed_to',
    ];

    public function borrower()
    {
        return $this->belongsTo(Client::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function getDatesAttribute()
    {
        return sprintf('%s - %s', $this->borrowed_from, $this->borrowed_to);
    }
}
