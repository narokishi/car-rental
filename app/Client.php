<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'pesel',
        'address_1',
        'address_2',
        'address_3',
        'postal_code',
        'city',
        'email',
        'phone_number',
    ];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'owner_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'borrower_id');
    }

    public function getNameAttribute()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    public function getAddressAttribute()
    {
        $pattern = $this->address_3 ? '%s %s/%s' : '%s %s';

        return sprintf($pattern, $this->address_1, $this->address_2, $this->address_3);
    }
}
