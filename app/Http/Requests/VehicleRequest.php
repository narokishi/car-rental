<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use InvalidArgumentException;
use App\Motorcycle;
use App\Truck;
use App\Car;

class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function getVehicle()
    {
        $vehicle = $this->getVehicleOfType();

        return $vehicle->fill($this->only([
            'vin',
            'plate_number',
            'mileage',
            'number_of_doors',
            'number_of_seats',
            'colour',
            'brand',
            'model',
            'subtype',
            'manufactured_at',
        ]));
    }

    private function getVehicleOfType()
    {
        switch ($this->input('type')) {
            case 'car':
                return new Car;
            case 'truck':
                return new Truck;
            case 'motorcycle':
                return new Motorcycle;
            default:
                throw new InvalidArgumentException;
        }
    }
}
