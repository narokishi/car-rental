<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Order;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_id'         => 'required',
            'borrower_id'        => 'required',
            'borrowed_from'      => 'required|array',
            'borrowed_from.date' => 'required|date',
            'borrowed_to'        => 'required|array',
            'borrowed_to.date'   => 'required|date',
        ];
    }

    public function getOrder()
    {
        $order = new Order;

        return $order->fill($this->getData());
    }

    public function getData()
    {
        return array_merge($this->only([
            'borrower_id',
            'vehicle_id',
        ]), $this->getDates());
    }

    private function getDates()
    {
        return $this->withValidation([
            'borrowed_from' => $this->toCarbon($this->input('borrowed_from')),
            'borrowed_to'   => $this->toCarbon($this->input('borrowed_to')),
        ]);
    }

    private function withValidation($array)
    {
        if ($array['borrowed_from']->diffInMinutes($array['borrowed_to'], false) <= 0) {\
            flash('Wprowadź daty chronologicznie')->error()->important();
            return $this->failedValidation($this->getValidatorInstance());
        }

        return $array;
    }

    private function toCarbon($array)
    {
        return Carbon::parse(sprintf('%s %s', $array['date'], $array['time']));
    }
}
