<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Client;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function getClient()
    {
        $client = new Client;

        return $client->fill($this->getData());
    }

    public function getData()
    {
        return $this->only([
            'first_name',
            'last_name',
            'pesel',
            'address_1',
            'address_2',
            'address_3',
            'postal_code',
            'city',
            'email',
            'phone_number',
        ]);
    }
}
