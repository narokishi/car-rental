<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use Illuminate\Http\Request;
use App\Vehicle;
use App\Client;
use App\Order;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::paginate(20);

        return view('order.index', [
            'orders' => $orders,
        ]);
    }

    public function create()
    {
        $vehicles = Vehicle::all()->pluck('name', 'id')->prepend('-', '');
        $clients  = Client::all()->pluck('name', 'id')->prepend('-', '');

        return view('order.create', [
            'vehicles' => $vehicles,
            'clients'  => $clients,
        ]);
    }

    public function store(OrderRequest $request)
    {
        $order = $request->getOrder();
        $order->save();

        return redirect()->route('order.index')->withFlash('Pomyślnie dodano zamówienie');
    }

    public function show(Order $order)
    {
        return view('order.show', [
            'order' => $order,
        ]);
    }

    public function edit(Order $order)
    {
        $vehicles = Vehicle::all()->pluck('name', 'id')->prepend('-', '');
        $clients  = Client::all()->pluck('name', 'id')->prepend('-', '');

        return view('order.edit', [
            'vehicles' => $vehicles,
            'clients'  => $clients,
            'order'    => $order,
        ]);;
    }

    public function update(Request $request, Order $order)
    {
        $order->update($request->getData());

        return redirect()->route('order.index')->withFlash('Pomyślnie zaktualizowano zamówienie');
    }

    public function destroy(Order $order)
    {
        $order->delete();

        return redirect()->route('order.index')->withFlash('Pomyślnie usunięto zamówienie');
    }
}
