<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::paginate(20);

        return view('client.index', [
            'clients' => $clients,
        ]);
    }

    public function create()
    {
        return view('client.create');
    }

    public function store(ClientRequest $request)
    {
        $client = $request->getClient();
        $client->save();

        return redirect()->route('client.index')->withFlash('Pomyślnie dodano klienta');
    }

    public function show(Client $client)
    {
        return view('client.show', [
            'client' => $client,
        ]);
    }

    public function edit(Client $client)
    {
        return view('client.edit', [
            'client' => $client,
        ]);
    }

    public function update(ClientRequest $request, Client $client)
    {
        $client->update($request->getData());

        return redirect()->route('client.index')->withFlash('Pomyślnie zaktualizowano klienta');
    }

    public function destroy(Client $client)
    {
        $client->delete();

        return redirect()->route('client.index')->withFlash('Pomyślnie usunięto klienta');
    }
}
