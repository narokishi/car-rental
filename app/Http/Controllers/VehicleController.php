<?php

namespace App\Http\Controllers;

use App\Http\Requests\VehicleRequest;
use Illuminate\Http\Request;
use App\Vehicle;
use App\Engine;
use App\Client;

class VehicleController extends Controller
{
    public function index()
    {
        $brand = request()->get('brand');
        $vehicles = $brand ? Vehicle::where('brand', $brand)->paginate(20) : Vehicle::paginate(20);

        return view('vehicle.index', [
            'vehicles' => $vehicles,
        ]);
    }

    public function create()
    {
        $types   = Vehicle::getTypes()->prepend('-', '');
        $engines = Engine::all()->prepend('-', '');

        return view('vehicle.create', [
            'types'   => $types,
            'engines' => $engines,
        ]);
    }

    public function store(VehicleRequest $request)
    {
        $vehicle = $request->getVehicle();
        $vehicle->save();

        return redirect()->route('vehicle.index')->withFlash('Pomyślnie dodano pojazd');
    }

    public function show(Vehicle $vehicle)
    {
        return view('vehicle.show', [
            'vehicle' => $vehicle,
        ]);
    }

    public function edit(Vehicle $vehicle)
    {
        $clients = Client::all()->pluck('name', 'id')->prepend('-', '');

        return view('vehicle.edit', [
            'vehicle' => $vehicle,
            'clients' => $clients,
        ]);
    }

    public function update(Request $request, Vehicle $vehicle)
    {
        $vehicle->update($request->only([
            'owner_id',
            'mileage',
        ]));

        return redirect()->route('vehicle.index')->withFlash('Pomyślnie zaktualizowano pojazd');
    }

    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();

        return redirect()->route('vehicle.index')->withFlash('Pomyślnie usunięto pojazd');
    }
}
