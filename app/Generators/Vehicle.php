<?php

namespace App\Generators;

use Carbon\Carbon;

class Vehicle
{
    private static $cars = [
        'Audi' => [
            'A4',
            'A6',
            'A8',
            'Q3',
            'Q5',
            'Q7',
        ],
        'BMW' => [
            'M3',
            'M4',
            'M5',
            'X3',
            'X4',
            'X5',
        ],
        'Opel' => [
            'Astra',
            'Zafira',
            'Omega',
            'Mokka',
        ],
        'Mercedes-Benz' => [
            'B W246',
            'B W245',
            'CLS',
        ],
        'Honda' => [
            'Civic',
            'CR-V',
            'CR-Z',
            'Accord',
            'CSX',
            'Stream',
        ],
        'Toyota' => [
            'Yaris',
            'Auris',
            'Avensis',
            'RAV4',
        ],
    ];

    private static $colours = [
        'Czarny',
        'Biały',
        'Niebieski',
        'Brązowy',
        'Zielony',
        'Srebrny',
    ];

    public static function random()
    {
        $brand   = array_rand(static::$cars, 1);
        $model   = array_rand(array_flip(static::$cars[$brand]), 1);
        $date    = Carbon::parse('2007-01-01')->addDays(rand(0, 3600));
        $mileage = rand(0, $date->diffInYears() * rand(5000, 15000));

        return [
            'brand'  => $brand,
            'model'  => $model,
            'colour' => array_rand(array_flip(static::$colours), 1),
            'manufactured_at' => $date,
            'mileage'         => $mileage,
        ];
    }
}
