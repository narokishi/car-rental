<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Truck extends Vehicle
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vehicles';

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'type' => 'truck',
    ];
}
