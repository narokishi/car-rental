<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engine extends Model
{
    protected $fillable = [
        'horsepower',
        'capacity',
        'number_of_gears',
        'type',
    ];

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class);
    }
}
