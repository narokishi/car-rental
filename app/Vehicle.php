<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Vehicle extends Model
{
    protected $fillable = [
        // Relationships
        'owner_id',
        'engine_id',
        'policy_id',
        // Descriptive attributes
        'vin',
        'plate_number',
        'mileage',
        'number_of_seats',
        'number_of_doors',
        'colour',
        'brand',
        'model',
        'type',
        'subtype',
        // Timestamps
        'manufactured_at',
    ];

    protected $with = [
        'owner',
        'orders',
    ];

    protected $dates = [
        'manufactured_at',
    ];

    public function owner()
    {
        return $this->belongsTo(Client::class);
    }

    public function engine()
    {
        return $this->belongsTo(Engine::class);
    }

    public function policy()
    {
        return $this->belongsTo(Policy::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function order()
    {
        return $this->hasOne(Order::class);
    }

    public static function getTypes()
    {
        return collect([
            'car'        => 'Samochód',
            'truck'      => 'Ciężarówka',
            'motorcycle' => 'Motocykl',
        ]);
    }

    public function getNameAttribute()
    {
        return sprintf('%s %s', $this->brand, $this->model);
    }

    public function isBorrowed()
    {
        return $this->orders->sortByDesc('borrowed_from')->map(function ($order) {
            return $order->borrowed_to->isPast() ? false : $order->borrowed_from->isPast();
        })->filter()->isNotEmpty();
    }

    public function getBorrowerAttribute()
    {
        return $this->isBorrowed() ? $this->orders->sortByDesc('borrowed_from')->first()->borrower : false;
    }
}
