<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            // Foreign keys
            $table->unsignedInteger('borrower_id')->nullable();
            $table->unsignedInteger('owner_id')->nullable();
            $table->unsignedInteger('engine_id')->nullable();
            $table->unsignedInteger('policy_id')->nullable();
            // Descriptive attributes
            $table->string('vin');
            $table->string('plate_number')->nullable();
            $table->unsignedInteger('mileage')->default(0);
            $table->unsignedInteger('number_of_seats');
            $table->unsignedInteger('number_of_doors')->nullable();
            $table->string('colour');
            $table->string('brand');
            $table->string('model');
            $table->string('type');
            $table->string('subtype')->nullable();
            // Timestamps
            $table->timestamp('manufactured_at')->nullable();
            $table->timestamps();
            // Foreigns
            $table->foreign('borrower_id')->references('id')->on('clients')
                ->onUpdate('cascade')->onDelete('set null');
            $table->foreign('owner_id')->references('id')->on('clients')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('engine_id')->references('id')->on('engines')
                ->onUpdate('cascade')->onDelete('restrict');
            // $table->foreign('policy_id')->references('id')->on('polices')
            //     ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
