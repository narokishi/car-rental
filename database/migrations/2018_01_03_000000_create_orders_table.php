<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            // Descriptive attributes
            $table->unsignedInteger('borrower_id')->nullable();
            $table->unsignedInteger('vehicle_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            // Timestamps
            $table->timestamp('borrowed_from')->nullable();
            $table->timestamp('borrowed_to')->nullable();
            $table->timestamps();
            // Foreigns
            $table->foreign('borrower_id')->references('id')->on('clients')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('vehicle_id')->references('id')->on('vehicles')
                ->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
