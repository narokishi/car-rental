<?php

use Faker\Generator as Faker;
use App\Generators\Vehicle as Generator;
use App\Vehicle;

$factory->define(Vehicle::class, function (Faker $faker) {
    $car = Generator::random();

    return [
        'vin'             => mb_strtoupper(str_random(17)),
        'number_of_seats' => rand(4, 5),
        'number_of_doors' => rand(3, 5),
        'manufactured_at' => $car['manufactured_at'],
        'mileage'         => $car['mileage'],
        'brand'           => $car['brand'],
        'model'           => $car['model'],
        'type'            => 'car',
        'colour'          => $car['colour'],
    ];
});
